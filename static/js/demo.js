var Demo = (function() {

	function output(node) {
		var existing = $('#result .croppie-result');
		if (existing.length > 0) {
			existing[0].parentNode.replaceChild(node, existing[0]);
		}
		else {
			$('#result')[0].appendChild(node);
		}
	}

	function popupResult(result) {
		var html;
		if (result.html) {
			html = result.html;
		}
		if (result.src) {
			html = '<img src="' + result.src + '" />';
		}
		swal({
			title: '',
			html: true,
			text: html,
			allowOutsideClick: true
		});
		setTimeout(function(){
			$('.sweet-alert').css('margin', function() {
				var top = -1 * ($(this).height() / 2),
					left = -1 * ($(this).width() / 2);

				return top + 'px 0 0 ' + left + 'px';
			});
		}, 1);
	}

	function demoMain () {
		var mc = $('#cropper-1');
		mc.croppie({
			viewport: {
				width: 150,
				height: 150,
				type: 'circle'
			},
			boundary: {
				width: 300,
				height: 300
			},
			// url: 'demo/demo-1.jpg',
			// enforceBoundary: false
			// mouseWheelZoom: false
		});
		mc.on('update.croppie', function (ev, data) {
			// console.log('jquery update', ev, data);
		});
		$('.js-main-image').on('click', function (ev) {
            mc.croppie('result', {
				type: 'rawcanvas',
				circle: true,
				// size: { width: 300, height: 300 },
            	format: 'png'
            }).then(function (canvas) {
				popupResult({
					src: canvas.toDataURL()
				});
			});
		});
	}

	function demoBasic() {
		var $w = $('.basic-width'),
			$h = $('.basic-height'),
			basic = $('#demo-basic').croppie({
			viewport: {
				width: 150,
				height: 200
			},
			boundary: {
				width: 300,
				height: 300
			}
		});
		basic.croppie('bind', {
			url: 'demo/cat.jpg',
			points: [77,469,280,739]
		});

		$('.basic-result').on('click', function() {
			var w = parseInt($w.val(), 10),
				h = parseInt($h.val(), 10),s
				size = 'viewport';
			if (w || h) {
				size = { width: w, height: h };
			}
			basic.croppie('result', {
				type: 'canvas',
				size: size,
				resultSize: {
					width: 50,
					height: 50
				}
			}).then(function (resp) {
				popupResult({
					src: resp
				});
			});
		});
	}

	function demoVanilla() {
		var vEl = document.getElementById('vanilla-demo'),
			vanilla = new Croppie(vEl, {
			viewport: { width: 200, height: 100 },
			boundary: { width: 300, height: 300 },
			showZoomer: false,
            enableOrientation: true
		});
		vanilla.bind({
            url: 'demo/demo-2.jpg',
            orientation: 4,
            zoom: 0
        });
        vEl.addEventListener('update', function (ev) {
        	// console.log('vanilla update', ev);
        });
		document.querySelector('.vanilla-result').addEventListener('click', function (ev) {
			vanilla.result({
				type: 'blob'
			}).then(function (blob) {
				popupResult({
					src: window.URL.createObjectURL(blob)
				});
			});
		});

		$('.vanilla-rotate').on('click', function(ev) {
			vanilla.rotate(parseInt($(this).data('deg')));
		});
	}

    function demoResizer() {
		var vEl = document.getElementById('resizer-demo'),
			resize = new Croppie(vEl, {
			viewport: { width: 100, height: 100 },
			boundary: { width: 300, height: 300 },
			showZoomer: false,
            enableResize: true,
            enableOrientation: true,
            mouseWheelZoom: 'ctrl'
		});
		resize.bind({
            url: 'demo/demo-2.jpg',
            zoom: 0
        });
        vEl.addEventListener('update', function (ev) {
        	console.log('resize update', ev);
        });
		document.querySelector('.resizer-result').addEventListener('click', function (ev) {
			resize.result({
				type: 'blob'
			}).then(function (blob) {
				popupResult({
					src: window.URL.createObjectURL(blob)
				});
			});
		});
	}

	function demoUpload() {
		var $uploadCrop;

		function readFile(input) {
 			if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            
	            reader.onload = function (e) {
					$('.upload-demo').addClass('ready');
	            	$uploadCrop.croppie('bind', {
						url: e.target.result,
						zoom: 0
	            	}).then(function(){
	            		console.log('jQuery bind complete');
	            	});
	            	
	            }
	            
	            reader.readAsDataURL(input.files[0]);
	        }
	        else {
		        swal("Sorry - you're browser doesn't support the FileReader API");
		    }
		}

		$uploadCrop = $('#upload-demo').croppie({
			viewport: {
				width: 400,
				height: 400
			},
			enableExif: true
		});

		$('#upload').on('change', function () { readFile(this); });
		$('.upload-result').on('click', function (ev) {
			$uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'viewport',
				format: 'jpeg'
			}).then(function (resp) {
				//console.log(resp);
				putb64(resp);
				// popupResult({
				// 	src: resp
				// });
			});
		});
	}

	function demoHidden() {
		var $hid = $('#hidden-demo');

		$hid.croppie({
			viewport: {
				width: 175,
				height: 175,
				type: 'circle'
			},
			boundary: {
				width: 200,
				height: 200
			}
		});
		$hid.croppie('bind', 'demo/demo-3.jpg');
		$('.show-hidden').on('click', function () {
			$hid.toggle();
			$hid.croppie('bind');
		});
	}

	function bindNavigation () {
		var $html = $('html');
		$('nav a').on('click', function (ev) {
			var lnk = $(ev.currentTarget),
				href = lnk.attr('href'),
				targetTop = $('a[name=' + href.substring(1) + ']').offset().top;

			$html.animate({ scrollTop: targetTop });
			ev.preventDefault();
		});
	}

	function init() {
		// bindNavigation();
		// demoMain();
		// demoBasic();	
		// demoVanilla();	
		// demoResizer();
		demoUpload();
		getUserImagesFromRemote();
		// uploadUserImageName();
		// demoHidden();
	}

	function putb64(base64) {
		var pic = base64.replace(/^.*?,/, '');
		$.ajax({
			type: 'post',
			url: "/index/getUptoken",
			success: function (data) {
				var token = data.data.uptoken;
				console.log(data);
				// 把字符串转换成json
				function strToJson(str) {
					var json = eval('(' + str + ')');
					return json;
				}
				var url = "https://up-z1.qiniup.com/putb64/-1"
				// var url = "https://cdn.51yingdian.com/putb64/-1/";
				var xhr = new XMLHttpRequest();
				xhr.onreadystatechange = function () {
					console.log(xhr.responseText);
					if (xhr.readyState == 4) {
						var keyText = xhr.responseText;
						// 返回的key是字符串，需要装换成json
						keyText = strToJson(keyText);
						console.log(keyText);
						//keyText.key 是返回的图片文件名
						var key = keyText.key;
						console.log({
							userInfoId: userInfoId,
							imgHash: key,
						});
						// var form = new FormData();
						// form.append("userInfoId", userInfoId);
						// form.append("imgHash", key);

						// var settings = {
						// 	"async": true,
						// 	"crossDomain": true,
						// 	"url": "https://api.51yingdian.com/shopkeeper/upload/img",
						// 	"method": "POST",
						// 	"headers": {
						// 		"Content-Type": "application/json",
						// 		"Cache-Control": "no-cache"
						// 	},
						// 	"processData": false,
						// 	"contentType": false,
						// 	"mimeType": "multipart/form-data",
						// 	"data": form
						// }

						// $.ajax(settings).done(function (response) {
						// 	console.log(response);
						// });
						uploadUserImageName(keyText.key);
					}
				}
				xhr.open("POST", url, true);
				xhr.setRequestHeader("Content-Type", "application/octet-stream");
				xhr.setRequestHeader("Authorization", "UpToken " + token);
				xhr.send(pic);
			},
			error: function () {
				console.log("sendFile请求出错");
			}
		})
	}

	function renderUserImages(list) {
		$("#userImageDiv").empty();
		for (var i=0; i< list.length; i++) {
			var image = document.createElement('img');
			image.src = list[i];
			$("#userImageDiv").append(image);
		}
	}

	function uploadUserImageName(namekey) {
		$.ajax({
			type: 'POST',
			url: "/index/uploadUserName",
			data:{
				userInfoId: userInfoId,
				imgHash: namekey,
			},
			success: function (data) {
				console.log(data);
				if (data && data.data) {
					renderUserImages(data.data);
				}
			},
			error: function () {
				
			}
		})
	}

	function getUserImagesFromRemote() {
		$.ajax({
			type: 'GET',
			url: "/index/uploadUserName?" +"userInfoId="+userInfoId,
			success: function (data) {
				console.log(data);
				if (data && data.data) {
					renderUserImages(data.data);
				}
			},
			error: function () {
				
			}
		})
	}

	return {
		init: init
	};
})();


// Full version of `log` that:
//  * Prevents errors on console methods when no console present.
//  * Exposes a global 'log' function that preserves line numbering and formatting.
(function () {
  var method;
  var noop = function () { };
  var methods = [
      'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
      'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
      'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
      'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});
 
  while (length--) {
    method = methods[length];
 
    // Only stub undefined methods.
    if (!console[method]) {
        console[method] = noop;
    }
  }
 
 
  if (Function.prototype.bind) {
    window.log = Function.prototype.bind.call(console.log, console);
  }
  else {
    window.log = function() { 
      Function.prototype.apply.call(console.log, console, arguments);
    };
  }
})();
