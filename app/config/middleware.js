/**
 * Middleware config
 * @return
 */
module.exports = { 
    list: ['view', 'session'], //加载的中间件列表
    config: { //中间件配置 
        static: {
            cache: false,
            preload: false
        },
        view: {
            view_path: process.env.ROOT_PATH + '/static/views/', //模板目录
            default_theme: '', //默认模板主题
        },
        session: {
            session_type: 'file', //数据缓存类型 file,redis,memcache
            session_name: 'crop_demo', //session对应的cookie名称
            session_key_prefix: 'crop_demo:', //session名称前缀
            session_options: {}, //session对应的cookie选项
            session_sign: '', //session对应的cookie使用签名
            session_timeout: 3600, //服务器上session失效时间，单位：秒
            // session对应的cookie配置
            cookie_option: { 
                domain: '', //cookie所在的域名
                path: '/', // cookie所在的路径
                // maxAge: -1, // cookie有效时长
                httpOnly: true, // 是否只用于http请求中获取
            },

            //session_type=file
            file_suffix: '.json', //File缓存方式下文件后缀名
            file_path: process.env.ROOT_PATH + '/cache',
        },
    }
};