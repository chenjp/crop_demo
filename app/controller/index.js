/**
 * Controller
 * @return
 */
const {controller, helper} = require('thinkkoa');
const qiniu_api = require('../service/qiniu_api');
const requestp = require('request-promise');
module.exports = class extends controller {
    //构造方法init代替constructor
    init(ctx, app){
        super.init(ctx, app);
        this.qiniuApi = new qiniu_api(app);
    }
    //所有该控制器(含子类)方法前置方法
    __before(){
        console.log('__before');
    }
    //URI定位到该控制器,如果该控制器不存在某个方法时自动调用
    __empty(){
        return this.write('can\'t find action');
    }
    //indexAction前置方法
    _before_index(){
        console.log('_before_index');
    }
    //控制器默认方法
    indexAction () {
        // return this.write('Hello, ThinkKoa!!');
        this.assign('userInfoId', this.param('id') || 0);
        this.ctx.set('Access-Control-Allow-Origin', '*');
        return this.render();
    }

    /**
     * 获取七牛uptoken
     */
    async getUptokenAction() {
        let tokenDic = await this.session('qiniu_token');
        let token = '';
        if (helper.isEmpty(tokenDic)) {
            token = this.qiniuApi.getUptoken();
            this.session('qiniu_token', {'uptoken': token});
        } else {
            token = tokenDic['uptoken'];
        }
        
        return this.ok('success', {'uptoken': token});
    }

    async uploadUserNameAction() {
        let userInfoId = this.param('userInfoId');
        let res = [];
        if (this.isPost()) {
            let imgHash = this.param('imgHash');
            let result = await requestp({
                uri: 'https://api.51yingdian.com/shopkeeper/upload/img',
                method: 'POST',
                form: {
                    userInfoId: userInfoId,
                    imgHash: imgHash
                },
                json: true,
                timeout: 10000
            }).catch(e => {
                echo(e.stack);
                return {};
            });

            if (result && result.data && result.data.body) {
                try {
                    res = JSON.parse(result.data.body);
                } catch (error) {
                    res = [];
                }
            }
        } else {
            let result = await requestp({
                uri: `https://api.51yingdian.com/shopkeeper/upload/img/${userInfoId}`,
                method: 'GET',
                json: true,
                timeout: 10000
            }).catch(e => {
                echo(e.stack);
                return {};
            });
            if (result && result.body) {
                try {
                    res = JSON.parse(result.body);
                } catch (error) {
                    res = [];
                }
            }
        }
        return this.ok('success', res);
    }
};